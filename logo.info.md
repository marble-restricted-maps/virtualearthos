The logo has been created on 2023-07-17 on the webservice "https://www.craiyon.com/" (AI Art Generator, V3) by entering the prompt "A clipart with a small map".

According to the policies and terms of craiyon.com which were in effect on 2023-07-17, the generated image may be used as a logo for a non commercial project:

https://www.craiyon.com/pricing, retrieved on 2023-07-17, states:

> *Yes, as long as you respect the [Terms of Use](https://www.craiyon.com/terms), feel free to use them as you wish for personal, academic or commercial use!*  
> *Please credit craiyon.com for the images if you are a free subscriber.*

And the "Terms of Use" (https://www.craiyon.com/terms), retrieved on 2023-07-17, can be read in the file [`logo.terms-of-use.txt`](logo.terms-of-use.txt) in this repository.
